extern crate itertools;
use std::collections::BTreeMap;
use std::fs;

fn main() {
    let contents = fs::read_to_string("data/13.in").expect("error reading file");

    let mut grid = [[' '; 150]; 150];

    let mut cars: BTreeMap<(usize, usize), (char, char)> = BTreeMap::new();

    let dirs = ['v', '^', '>', '<'];

    for (row, line) in contents.lines().enumerate() {
        for (col, c) in line.chars().enumerate() {
            if c != ' ' {
                if dirs.contains(&c) {
                    cars.insert((row, col), (c, '<'));
                    if c == '>' || c == '<' {
                        grid[row][col] = '-';
                    } else {
                        grid[row][col] = '|';
                    }
                } else {
                    grid[row][col] = c;
                }
            }
        }
    }

    fn make_turn(dir: char, turn: char) -> (char, char) {
        match dir {
            '>' => match turn {
                '<' => ('^', '^'),
                '^' => ('>', '>'),
                '>' => ('v', '<'),
                _ => panic!("unknown turn"),
            },
            'v' => match turn {
                '<' => ('>', '^'),
                '^' => ('v', '>'),
                '>' => ('<', '<'),
                _ => panic!("unknown turn"),
            },
            '<' => match turn {
                '<' => ('v', '^'),
                '^' => ('<', '>'),
                '>' => ('^', '<'),
                _ => panic!("unknown turn"),
            },
            '^' => match turn {
                '<' => ('<', '^'),
                '^' => ('^', '>'),
                '>' => ('>', '<'),
                _ => panic!("unknown turn"),
            },
            _ => panic!("unknown direction"),
        }
    }

    let (y, x) = loop {
        for (&(row, col), &(dir, turn)) in &cars.clone() {
            if cars.remove(&(row, col)).is_some() {
                match dir {
                    '>' => {
                        let col = col + 1;

                        let next_state = match grid[row][col] {
                            '-' => ('>', turn),
                            '\\' => ('v', turn),
                            '/' => ('^', turn),
                            '+' => make_turn(dir, turn),
                            __ => panic!("Unexpected element: {},{} => {:?}", row, col, __),
                        };

                        if cars.insert((row, col), next_state).is_some() {
                            cars.remove(&(row, col));
                        }
                    }
                    'v' => {
                        let row = row + 1;

                        let next_state = match grid[row][col] {
                            '|' => ('v', turn),
                            '\\' => ('>', turn),
                            '/' => ('<', turn),
                            '+' => make_turn(dir, turn),
                            __ => panic!("Unexpected element: {},{} => {:?}", row, col, __),
                        };

                        if cars.insert((row, col), next_state).is_some() {
                            cars.remove(&(row, col));
                        }
                    }
                    '<' => {
                        let col = col - 1;

                        let next_state = match grid[row][col] {
                            '-' => ('<', turn),
                            '\\' => ('^', turn),
                            '/' => ('v', turn),
                            '+' => make_turn(dir, turn),
                            __ => panic!("Unexpected element: {},{} => {:?}", row, col, __),
                        };

                        if cars.insert((row, col), next_state).is_some() {
                            cars.remove(&(row, col));
                        }
                    }
                    '^' => {
                        let row = row - 1;

                        let next_state = match grid[row][col] {
                            '\\' => ('<', turn),
                            '/' => ('>', turn),
                            '|' => ('^', turn),
                            '+' => make_turn(dir, turn),
                            __ => panic!("Unexpected element: {},{} => {:?}", row, col, __),
                        };

                        if cars.insert((row, col), next_state).is_some() {
                            cars.remove(&(row, col));
                        }
                    }
                    _ => panic!("unknown direction"),
                }
            }
        }

        if cars.len() == 1 {
            break cars.keys().next().unwrap();
        }
    };

    println!("{},{}", x, y);
}
